package locadora.entidade;

public class Veiculo {
	
	private int anoFabricacao;
	
	private String placa;
	
	private float valor;
	
	private Motorista motorista;
	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public Motorista getMotorista() {
		return motorista;
	}

	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}

	public int getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(int anoFabricacao) {
		if(anoFabricacao > 1970)
			this.anoFabricacao = anoFabricacao;
		else
			throw new Error("Ano Invalido " + anoFabricacao);	
	}


}
