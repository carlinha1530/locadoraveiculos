package locadora.entidade;

public class Locacao {

	private Pessoa p;
	private Veiculo v;
	
	public Pessoa getP() {
		return p;
	}
	public void setP(Pessoa p) {
		this.p = p;
	}
	public Veiculo getV() {
		return v;
	}
	public void setV(Veiculo v) {
		this.v = v;
	}
	
	
}
