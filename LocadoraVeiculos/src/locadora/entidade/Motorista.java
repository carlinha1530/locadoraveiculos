package locadora.entidade;

public class Motorista extends Pessoa{

	private String CNH;

	public String getCNH() {
		return CNH;
	}

	public void setCNH(String cNH) {
		CNH = cNH;
	}
	
	public String toString() {
		return "Motorista: " + getNome() + " " + CNH;				
	}
	
	
}
