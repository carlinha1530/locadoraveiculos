package locadora.entidade;

public class Carro extends Veiculo {
	
	public String modelo;
	
		
	public float calculaIPVA() {
		System.out.println("Calculo IPVA comum");
		return getValor()/((2016-(getAnoFabricacao()))*2);
	}

	public String toString() {
		return "Carro:" + getPlaca() + " " + getValor() + " "
				+ getAnoFabricacao() + " "
				+ getMotorista();
	}
	
	
}
