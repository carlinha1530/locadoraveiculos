package locadora.tela;

import java.util.Scanner;
import locadora.entidade.Veiculo;
import locadora.persistencia.VeiculoCadastro;

public class VeiculoTela extends Menu {
	
	private static VeiculoCadastro cadastro = new VeiculoCadastro(); 
	
	
	public static void main(String args[]){
		VeiculoTela tela = new VeiculoTela();
		tela.mostraMenu();
	
	}
	
	public void mostraMenu(){
		int opcao = -1;	
	    String opcoes[] = {"Sair", "Cadastrar", "Listar"};
	    
		while(opcao!=0) {
			opcao = Menu.imprimeMenu(opcoes);
			switch(opcao) {
				case 1: cadatraVeiculo(); break;
				case 2: listarVeiculos(); break;
			}
		}	
	}
	
	public static void cadatraVeiculo(){
		Veiculo v = new Veiculo();
		System.out.println("Cadastro de veiculo");		
		System.out.println("anoFabricacao:");
		v.setAnoFabricacao(teclado.nextInt());
		teclado.nextLine(); // CORRECAO
		System.out.println("placa:");
		v.setPlaca(teclado.nextLine());
		System.out.println("valor:");
		v.setValor(teclado.nextFloat());
		cadastro.adiciona(v);
	}
	
	public static void listarVeiculos() {
		for(Veiculo v: cadastro.listarTodos()) {
			System.out.println(v.getAnoFabricacao());
			System.out.println(v.getPlaca());
			System.out.println(v.getValor());
		}
			
	}

}
