package locadora.tela;

import java.util.Scanner;

public class MenuPrincipal {
	
	public static Scanner teclado = new Scanner(System.in); 	
	
	public static void main(String[] args) {
	  MenuPrincipal m = new MenuPrincipal();

	  //adicionar menus ...	  
	  String[] opcoes = {"Veiculos", "Clientes"};
	  Menu[] menus = {new VeiculoTela(),
			  new ClienteTela() }; 
	  
	  m.MenuPrincipal(opcoes, menus);
	}

	

	public void MenuPrincipal(String[] opcoes, Menu[] menus) { 
		
		int opcao = 0;
		
		while(opcao!=-1) {
	     System.out.println("Menu Principal - Escolha uma opcao ou -1 para sair");
			
		for(int i=0; i<opcoes.length; i++) 
			System.out.println(i + ":" + opcoes[i]);
		    		    
		 opcao = teclado.nextInt();
		 if(opcao!=-1)
			 menus[opcao].mostraMenu();
		}
	}
	
}
	
	
	

